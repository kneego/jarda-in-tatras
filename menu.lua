local composer = require( "composer" )
local scene = composer.newScene()

local physics = require( "physics" )
local widget = require( "widget" )

local common = require( "common" )

local bg
local audioSwitch

function scene:create( event )
	utils.debug('menu:scene:create')

	-- initialize the scene
	local sceneGroup = self.view

	-- background 336 x 480 / screenH
	local mountain_background = display.newImageRect('images/home.jpg', 407 * (screenH / 480), screenH)
	mountain_background.x = halfW
	mountain_background.y = halfH
	sceneGroup:insert( mountain_background )

	-- play button
	local play_button = display.newImageRect( sceneGroup, 'images/play.png', 120, 120 )
	play_button.x = halfW
	play_button.y = screenH - 165
	play_button.anchorY = 1

	utils.handlerAdd( play_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( play_button )
		elseif event.phase == 'ended' and common.button_up( play_button ) then
			composer.gotoScene( 'game' )
		end

		return true
	end )

	-- leader board button
	local leaderBoard = display.newImageRect( sceneGroup, 'images/leaderboard.png', 60, 60 )
	leaderBoard.x = halfW - 80
	leaderBoard.y = screenH - 80
	leaderBoard.anchorY = 1
	sceneGroup:insert( leaderBoard )

	utils.handlerAdd( leaderBoard, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( leaderBoard )
		elseif event.phase == 'ended' and common.button_up( leaderBoard ) then
			game_network.show_leaderboards()
		end

		return true
	end )

	-- audio button
	audioSwitch = common.audioSwitchIcon()
	sceneGroup:insert( audioSwitch )

	if settings.sounds then
		audioSwitch:setSequence( 'on' )
	else
		audioSwitch:setSequence( 'off' )
	end
	audioSwitch:play( )

	utils.handlerAdd( audioSwitch, 'touch', function( self, event )
		if event.phase == 'began' then
			common.button_down( audioSwitch )
		elseif event.phase == 'ended' and common.button_up( audioSwitch ) then
			-- change sounds state
			if settings.sounds then
				settings.sounds = false
				audioSwitch:setSequence( 'off' )
			else
				settings.sounds = true
				audioSwitch:setSequence( 'on' )
			end

			utils.saveSettings( settings )
			audioSwitch:play( )
		end

		return true
	end )
	sceneGroup:insert( audioSwitch )

	-- info button
	local info_button = display.newImageRect( sceneGroup, 'images/info.png', 60, 60 )
	info_button.anchorY = 1
	info_button.x = halfW + 80
	info_button.y = screenH - 80
	sceneGroup:insert( info_button )

	utils.handlerAdd( info_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( info_button )
		elseif event.phase == 'ended' and common.button_up( info_button ) then
			composer.gotoScene( 'info' )
		end

		return true
	end )

	-- add FB and twitter buttons
	common.social_networks_buttons( sceneGroup )
end

function scene:show( event )
	if event.phase == 'will' then
		ads.load( "interstitial", { testMode = false } )

		ga:view('menu')
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)

	elseif phase == "did" then
		
	end	
	
end

function scene:destroy( event )
	local sceneGroup = self.view

end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene