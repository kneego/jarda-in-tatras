# Jarda in Tatras

Full source code and assets of Jarda in Tatras game for [Android](https://play.google.com/store/apps/details?id=org.keego.jardaintatras) and [iOS](https://itunes.apple.com/us/app/jarda-in-tatras/id1068434722?mt=8).

The game uses the [Corona SDK](https://coronalabs.com) game engine.
