local M = {}

M.EMBOSS_COLOR = {
	highlight = { r = 32 / 255, g = 70 / 255, b = 9 / 255 },
	shadow = { r = 32 / 255, g = 70 / 255, b = 9 / 255 }
}

M.DEFAULT_FONT = 'happysans'

function M.audioSwitchIcon()
	local imageSheetOptions = {
		width = 60,
		height = 60,
		numFrames = 2,
		sheetContentWidth = 120,
		sheetContentHeight = 60
	}

	local sheet = graphics.newImageSheet( 'images/audio.png', imageSheetOptions)
	
	local o = display.newSprite( sheet, {
		{name = 'on', start = 1, count = 1},
		{name = 'off', start = 2, count = 1}
	} )

	o.x = halfW
	o.y = screenH - 80
	o.anchorY = 1
	
	return o
end

function M.button_down( button )
	button.y = button.y + 2
	button.down = true

	display.currentStage:setFocus( button )

	sounds.play(sounds.sound.button_click)
end

function M.button_up( button )
	if button.down then
		button.y = button.y - 2
		button.down = false

		display.currentStage:setFocus( nil )

		return true
	else
		return false
	end
end

function M.social_networks_buttons( parent )
	-- facebook
	local facebook_button = display.newImageRect( parent, 'images/facebook.png', 32, 32 )
	facebook_button.alpha = 0.9
	facebook_button.x = screenW - 16 - 6
	facebook_button.y = halfH - 20

	utils.handlerAdd( facebook_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			M.button_down( facebook_button )
		elseif event.phase == 'ended' and M.button_up( facebook_button ) then
			system.openURL( R.strings('facebook_url') )
		end

		return true
	end )

	-- twitter
	local twitter_button = display.newImageRect( parent, 'images/twitter.png', 32, 32 )
	twitter_button.alpha = 0.9
	twitter_button.x = screenW - 16 - 6
	twitter_button.y = halfH + 20

	utils.handlerAdd( twitter_button, 'touch', function ( self, event )
		if event.phase == 'began' then
			M.button_down( twitter_button )
		elseif event.phase == 'ended' and M.button_up( twitter_button ) then
			system.openURL( R.strings('twitter_url') )
		end

		return true
	end )
end

function M.background()
	local background_width = 1077
	local background_height = 1024
	local width = screenH / 680 * background_width
	
	mountain_background = display.newImageRect( 'images/mountains.png', width, screenH + (background_height - 680) )
	mountain_background.anchorX = 0
	mountain_background.anchorY = 1
	mountain_background.y = screenH
	mountain_background.x = -math.random(width - screenW)

	return mountain_background
end

return M