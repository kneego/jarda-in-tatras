local _M = {}

_M.sound = {
	button_click = audio.loadSound( "sounds/button-click.wav" ),

	win = audio.loadSound( "sounds/win.mp3" ),

	jump = audio.loadSound( 'sounds/jump.mp3' ),
	hit = audio.loadSound( 'sounds/hit.mp3' ),
	collect = audio.loadSound( 'sounds/collect.mp3' ),
	haaa = audio.loadSound( 'sounds/haaa.mp3' ),
	
	helicopter = audio.loadSound( 'sounds/helicopter.mp3' ),
	ufo = audio.loadSound( 'sounds/ufo.mp3' ),

	helicopter_falling = audio.loadSound( 'sounds/helicopter-falling.mp3' ),
	tourist_falling = audio.loadSound( 'sounds/tourist-falling.mp3' ),
	rock_falling = audio.loadSound( 'sounds/rock-falling.mp3' ),

	-- music = audio.loadSound( "sounds/music.mp3" ),
}

_M.channel = {
	COMMON_CHANNEL = 1,
	SFX_CHANNEL = 2,
	PLAYER_CHANNEL = 3,
	MUSIC_CHANNEL = 4,
	HELICOPTER_CHANNEL = 5,
	FALLING_CHANNEL = 6,
}

function _M.play( sound, channel, loops)
	-- utils.debug('game:play_sound', loops)
	if settings.sounds then
		if channel == nil then channel = _M.channel.COMMON_CHANNEL end
		if loops == nil then loops = 0 end

		-- utils.debug('game:play_sound', channel)

		audio.stop( channel )

		audio.play( sound, {channel = channel, loops = loops} )
	end
end

function _M.stop( channel )
	if settings.sounds then
		audio.stop( channel )
	end
end

function _M.vibrate()
	if settings.vibrations then
		system.vibrate()
	end
end

return _M