local _M = {
	default = {},
	sk = {},
	cs = {},
}

_M.default.admob_id = {
	['iPhone OS'] = 'ca-app-pub-1210274935500874/1171430744',
	['Android'] = 'ca-app-pub-1210274935500874/7217964347'
}

_M.default.board_id = {
	['iPhone OS'] = 'topclimbers',
	['Android'] = 'CgkIj9vR0J8GEAIQAA',
}

_M.default.achievements = {
	['iPhone OS'] = {
		['200'] = '200m',
		['400'] = '400m',
		['600'] = '600m',
		['800'] = '800m',
		['1000'] = '1000m',
	},
	['Android'] = {
		['200'] = 'CgkIj9vR0J8GEAIQAQ',
		['400'] = 'CgkIj9vR0J8GEAIQAg',
		['600'] = 'CgkIj9vR0J8GEAIQAw',
		['800'] = 'CgkIj9vR0J8GEAIQBA',
		['1000'] = 'CgkIj9vR0J8GEAIQBQ',
	},
}

_M.default.barriers = {
	{ path='barrier-stone-1', width=48, height=54, type = 'stone-1' },
	{ path='barrier-stone-3', width=48, height=60, type = 'stone-3' },
	{ path='barrier-stone-4', width=60, height=40, type = 'stone-4' },

	{ path='barrier-wood-2', width=91, height=42, type = 'wood-2' },
	{ path='barrier-wood-5', width=95, height=27, type = 'wood-5' },
}

_M.default.clouds = {
	{ path='cloud-1.png', width=100, height=44 },
	{ path='cloud-2.png', width=100, height=44 },
	{ path='cloud-3.png', width=100, height=53 },
}

_M.default.info = {
	"The phone number for the Slovak Mountain Rescue service is 18300. You can also call the international number 112.",
	"When you are leaving for a hike let someone know where you're headed. Write your trip in the \"Hike Book\"",
	"You can lower the risk of meeting wildlife (i.e. a bear) by hiking in larger groups. Do not hike across valleys and bushes. Put a bell on your backpack.",
	"In case something happens to someone, first make sure you are ok and safe before helping others.",
	"Always hike in a group of three or more. In case of an accident, two people can carry the third injured person.",
	"Only hike on marked trails.",
	"A mountain sports insurance can save you a lot of money in case the Mountain Rescue Service has to be called.",
	"Don't underestimate the weather and always check it before leaving for a hike.",
	"If a storm is approaching hike down from the ridge.",
	"Don't hide under lone standing trees and krans. Only hide in caves if they are big enough so that you have at least 3m over your head and 1m from the walls.",
}

_M.sk.info = {
	'Telefónne číslo na slovenskú horskú zachrannú službu je 18300. Použiť môzeš aj medzinárodné číslo 112.',
	'Keď odchádzaš na túru, povedz niekomu doma, kam ideš. Zapíš sa do "Knihy vychádzok a túr".',
	'Riziko stretnutia s divou zverou, napr. medveďom, znížiš tak, že sa pohybuješ s väčšou skupinou. Nelez dolinami a kríkmi. Kúp si na batoh zvonček.',
	'Ak sa niekomu niečo stane, ako prvé sa uisti, že Ty si v bezpečí a že ti nič nehrozí.',
	'Na túru choďte aspoň traja. Pri nehode dvaja vždy vedia zniesť tretieho do bezpečia.',
	'Pohybuj sa iba po vyznačených turistických trasách.',
	'Poistenie na hory ti môže v prípade zásahu hroskou službou ušetriť veľa peňazí.',
	'Nepodceňuj počasie. Pred túrou si vždy pozri aktuálnu predpoveď.',
	'Keď sa blíži búrka, zídi z hrebeňa.',
	'Pri búrke sa neskrývaj pod osamelé stromy alebo skalné previsy. Do jaskyňe sa ukry iba ak je dosť veľká aby si mal nad hlavou aspoň 3m a od steny 1m priestoru.',
}

_M.cs.info = _M.sk.info

return _M
