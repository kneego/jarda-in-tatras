local _M = {}

_M.default = {
	jarda_in_tatras = 'Jarda in Tatras',
	
	-- menu
	play_label = 'Play',
	back_label = 'Back',
	menu_label = 'Menu',

	facebook_url = 'https://m.facebook.com/Kneego-123462307855125/',
	twitter_url = 'https://m.twitter.com/KneeGoApps',

	replay = 'Replay',
	leaderboard = 'Leaders',
	main_menu = 'Main menu',

	how_to_survive = 'How to survive...',
	altitude = 'Altitude: ',
	new_best_score = 'New top score!',
	score = 'Score:',
	your_best = 'Your best:',
}

_M.sk = {
	jarda_in_tatras = 'Jarda v Tatrách',

	replay = 'Hraj zonva',
	leaderboard = 'Naj hráči',
	main_menu = 'Menu',

	how_to_survive = 'Ako prežiť...',
	altitude = 'Výška: ',
	new_best_score = 'Nové top skóre!',
	score = 'Skóre:',
	your_best = 'Najlepšie:',
}

_M.cz = {
	jarda_in_tatras = 'Jarda v Tatrách',

    replay = 'Hraj zonvu',
	leaderboard = 'Nej hráči',
	main_menu = 'Menu',

    how_to_survive = 'Jak přežít...',
    altitude = 'Výška: ',
    new_best_score = 'Nové top skóre!',
    score = 'Skóre:',
	your_best = 'Nejlepší:',
}

return _M