-----------------------------------------------------------------------------------------
--
-- game.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local widget = require( "widget" )
local transition = require( "transition" )
local math = require( "math" )
local physics = require( "physics" )
local audio = require('audio')

local common = require( 'common' )
local shake = require( 'knee.shake' )

--------------------------------------------

-- pozadie
local background_group
local mountain_background
local mountain_background_height

-- steny
local wall_left_1
local wall_left_2
local wall_right_1
local wall_right_2

local barrier_group
local game_running
local skip_barier_fragment

-- lezec
local climber
local climber_group

-- kvapky
local aqua
local front_group
local back_group
local create_aqua_timer

-- rychlost posuvania
local start_speed = 6
local max_speed = 14
local speed = start_speed
local change_speed_flag

-- generovanie
local number_of_fragments
local wall_position
local barrier_space
local current_barrier_space
local start_barrier_space = 250
local min_barrier_space = 180
local start_cloud_position = 40

-- score
local score
local last_score
local score_text

-- timers
local emit_helicopter_timer
local emit_helmet_timer
local emit_falling_objet_timer
local emit_cloud_timer
local emit_jelen_timer

-- user interface
local helmet_status_on
local helmet_status_off

-- helmet
local have_helmet = false

-- images
local helicopter_image
local ufo_image
local falling_tourist_image
local falling_stone_big_image
local falling_stone_small_image
local helmet_image
local jelen_image

local physics_bodies
local physics_bodies_count

local barrier_definition = R.arrays('barriers')
local barrier_image
local all_barriers
local all_barriers_count
local last_barrier

local small_aqua
local big_aqua
local aqua_count

--------------------------------------------

function preload_images()
	helicopter_image = display.newImageRect(back_group, 'images/helicopter.png', 75, 44)
	ufo_image = display.newImageRect(back_group, 'images/ufo.png', 100, 46)

	falling_tourist_image = display.newImageRect(back_group, 'images/falling-tourist.png', 100, 103)
	falling_kozorozec_image = display.newImageRect(back_group, 'images/falling-kozorozec.png', 95, 108)
	falling_stone_big_image = display.newImageRect(back_group, 'images/falling-stone-1.png', 35, 35)
	falling_stone_small_image = display.newImageRect(front_group, 'images/falling-stone-2.png', 45, 45)

	jelen_image = display.newImageRect(front_group, 'images/jelen.png', 87, 75)

	helmet_image = display.newImageRect( back_group, 'images/helmet.png', 50, 52 )
	helmet_image.object_name = 'helmet'

	physics_bodies = {
		falling_tourist_image,
		falling_kozorozec_image,
		falling_stone_big_image,
		falling_stone_small_image,
		helmet_image,
	}

	-- kvpky tiez preloadneme
	small_aqua = {}
	big_aqua = {}
	for i = 1, 5 do
		local aqua = display.newImageRect( back_group, 'images/aqua-small.png', 14, 30 )
		aqua.isFixedRotation = true
		small_aqua[#small_aqua + 1] = aqua
		physics_bodies[#physics_bodies + 1] = aqua

		local aqua = display.newImageRect( front_group, 'images/aqua-big.png', 20, 40 )
		aqua.isFixedRotation = true
		big_aqua[#big_aqua + 1] = aqua
		physics_bodies[#physics_bodies + 1] = aqua
	end

	aqua_count = #big_aqua
	physics_bodies_count = #physics_bodies

	-- vytvorime bariery
	all_barriers = {}
	local left_barrier_image = {}
	local right_barrier_image = {}
	
	for i = 1, #barrier_definition do
		-- vytovorime tento typ bariery
		local definition = barrier_definition[i]

		left_barrier_image[definition.type] = {}
		right_barrier_image[definition.type] = {}

		for j = 1, 3 do
			-- pre kazdy typ vytvorime 3 itemy lave
			local object = display.newImageRect( barrier_group, 'images/' .. definition.path .. '-left.png', definition.width, definition.height )
			object.anchorX = 0
			object.y = -100
			object.x = 45

			object.object_name = 'barrier'
			object.wall = 'left'
			object.is_available = true
			object.gravityScale = 0
			
			left_barrier_image[definition.type][j] = object
			all_barriers[#all_barriers + 1] = object

			-- pre kazdy typ vytvorime 3 itemy prave
			local object = display.newImageRect( barrier_group, 'images/' .. definition.path .. '-right.png', definition.width, definition.height )
			object.anchorX = 1
			object.y = - 100
			object.x = screenW - 45

			object.object_name = 'barrier'
			object.wall = 'right'
			object.is_available = true
			object.gravityScale = 0
			
			right_barrier_image[definition.type][j] = object
			all_barriers[#all_barriers + 1] = object
		end
	end

	-- aby sme nemuseli pocet zistovat kazdy frame
	all_barriers_count = #all_barriers

	-- obsahuje vseky bariery
	barrier_image = {
		left = left_barrier_image,
		right = right_barrier_image
	}
end

function reset_physics_body()
	if game_running == false then return end

	local off_screen_limit = screenH + 50

	for i = 1, physics_bodies_count do
		local object = physics_bodies[i]
		if object.isBodyActive and object.y > off_screen_limit then
			physics.removeBody( object )
			object.y = -100
		end
	end

	timer.performWithDelay(1000, reset_physics_body)
end

function init_physics_body()
	for i = 1, physics_bodies_count do
		physics_bodies[i].y = -100
	end
end

--------------------------------------------

local runtime = 0
local delte_time

function get_delta_time()
    local temp = system.getTimer()  -- Get current game time in ms
    local dt = (temp - runtime) / (1000 / 60)  -- 60 fps or 30 fps as base
    
    runtime = temp  -- Store game time
    
    return dt
end

--------------------------------------------

function update_helmet_indicator()
	if have_helmet then
		helmet_status_on.isVisible = true
	else
		helmet_status_on.isVisible = false
	end
end

function emit_helicopter()
	if not game_running then return end

	if score > 400 and score < 600 then
		-- pocas tohoto obdobia helikopteru neemitujeme
		emit_helicopter_timer = timer.performWithDelay(10000 + math.random(2000), emit_helicopter)
		return
	end

	local object
	local start
	local finish

	if score < 400 then
		sounds.play(sounds.sound.helicopter, sounds.channel.HELICOPTER_CHANNEL)
		object = helicopter_image
	else
		sounds.play(sounds.sound.ufo, sounds.channel.HELICOPTER_CHANNEL)
		object = ufo_image
	end

	if math.random(2) > 1 then
		-- pojdeme z prava do lava
		object.xScale = 1
		start = screenW + 50
		finish = -50 
	else
		-- pojdeme z lava do prava
		object.xScale = -1
		start = -50
		finish = screenW + 50
	end

	object:toFront()
	object.x = start
	object.y = 60 + (start_cloud_position / 2) + math.random(20)
	transition.to( object, {
		time = 2500,
		x = finish
	})

	emit_helicopter_timer = timer.performWithDelay(10000 + math.random(2000), emit_helicopter)
end

function emit_cloud()
	if not game_running then return end
	if cloud_position > screenH + 50 then end  -- tu uz oblaky nebudu

	local start
	local finish

	local selecected_cloud = R.arrays('clouds')[math.random(3)]
	local object = display.newImageRect(back_group, 'images/' .. selecected_cloud.path, selecected_cloud.width, selecected_cloud.height)

	object.x = screenW + 50
	object.y = cloud_position + math.random(100)

	local scale = math.random(600, 1000) / 1000
	object.xScale = scale
	object.yScale = scale

	transition.to( object, {
		time = 10000 + math.random(6000),
		x = -50,
		onComplete = function ( object )
			display.remove( object )
		end
	})

	emit_cloud_timer = timer.performWithDelay(2000 + math.random(1000), emit_cloud)
end

function falling_object()
	-- ak hra nebezi, nic nevytvarame
	if game_running == false then return end

	-- vytvorime novy dekorativny objekt
	local object
	local density

	local r = math.random(100)
	if r > 80 then
		-- turista
		object = falling_tourist_image
		density = 10

		sounds.play(sounds.sound.tourist_falling, sounds.channel.FALLING_CHANNEL)
	elseif r > 90 then
		object = falling_kozorozec_image
		density = 10

		sounds.play(sounds.sound.rock_falling, sounds.channel.FALLING_CHANNEL)
	else
		-- kamen
		if math.random(2) > 1 then
			object = falling_stone_big_image
		else
			object = falling_stone_small_image
		end

		density = 5

		sounds.play(sounds.sound.rock_falling, sounds.channel.FALLING_CHANNEL)
	end

	object.y = -100
	object.x = 60 + math.random(screenW - 120)

	physics.addBody( object, 'dynamic', {density = _d, friction = 0, bounce = 0} )

	-- zatrasieme screen
	shake.start()
	timer.performWithDelay( 400, function ( ... )
		shake.stop()
	end)

	-- neskor budeme vytvarat dalsi
	emit_falling_objet_timer = timer.performWithDelay(10000 + math.random(5000), falling_object)
end

function emit_helmet()
	-- ak hra nebezi, nic nevytvarame
	if game_running == false then return end

	if have_helmet == false then
		-- ak nema helmu, tak ju generujem
		helmet_image.y = -50

		if math.random(2) > 1 then
			helmet_image.anchorX = 0
			helmet_image.x = 68
		else
			helmet_image.anchorX = 1
			helmet_image.x = screenW - 68
		end

		physics.addBody( helmet_image, 'dynamic', {density = 6, friction = 0, bounce = 0, filter={
			categoryBits=4, maskBits=1
		}} )
	end

	-- neskor budeme vytvarat dalsi
	emit_helmet_timer = timer.performWithDelay(15000 + math.random(10000), emit_helmet)
end

function emit_jelen( ... )
	-- ak hra nebezi, nic nevytvarame
	if game_running == false then return end

	local _position = math.random(4)
	local source_x
	local source_y
	local target_x
	local target_y
	local rotation

	local h = 75 / 2

	if _position == 1 then
		-- lava strana
		source_x = -h
		source_y = math.random(screenH - 100) + 50
		target_x = h
		target_y = source_y
		rotation = 90
	elseif _position == 2 then
		-- prava strana
		source_x = screenW + h
		source_y = math.random(screenH - 100) + 50
		target_x = screenW - h
		target_y = source_y
		rotation = 270
	elseif _position == 3 then
		-- hore
		source_x = math.random(screenW - 100) + 50
		source_y = -h
		target_x = source_x
		target_y = h
		rotation = 180
	else
		-- dole
		source_x = math.random(screenW - 100) + 50
		source_y = screenH + h
		target_x = source_x
		target_y = screenH - h
		rotation = 0
	end

	if not jelen_out then
		-- jelena umiestnime na startovnu poziciu
		jelen_image.x = source_x
		jelen_image.y = source_y
		jelen_image.rotation = rotation

		-- najprv jelena vysunieme
		jelen_out = true
		transition.to( jelen_image, {
			x = target_x,
			y = target_y,
			time = 800,
			onComplete = function ( ... )
				-- ak hra nebezi, nic nerobime
				if game_running == false then return end

				timer.performWithDelay( 600, function ( ... )
					-- ak hra nebezi, nic nerobime
					if game_running == false then return end

					-- a teraz ho opat skovame
					transition.to( jelen_image, {
						x = source_x,
						y = source_y,
						time = 800,
						onComplete = function ( ... )
							jelen_out = false
						end
					} )
				end)
				
			end
		} )
	end

	-- neskor budeme vytvarat dalsi
	emit_jelen_timer = timer.performWithDelay(15000 + math.random(10000), emit_jelen)
end

function move_object( o, s )
	-- pozadia posuniem dole (simulujem, ze postava ide smerom hore)
	local delta = (s or speed) / 2

	o:translate( 0, delta * delta_time )

	-- ak je pozadie mimo screen, tak ho dam hore, aby som vytvoril loop
	_diff = o.y - screenH

	if _diff >= 0 then
		o.y = -screenH + _diff
	end
end

function move_background( )
	if mountain_background.y < mountain_background_height then
		mountain_background.y = mountain_background.y + speed / 100
	end

	cloud_position = cloud_position + speed / 120
end

function move_wall( )
	move_object(wall_left_1)
	move_object(wall_left_2)

	move_object(wall_right_1)
	move_object(wall_right_2)
end

function move_barrier()
	-- posuvame vsetky aktivne bariery
	local remove_limit = screenH + 50
	for i = 1, all_barriers_count do
		if all_barriers[i].is_available == false then
			if all_barriers[i].y > remove_limit then
				-- tento objekt vyhodime, lebo je mimo obrazovky
				all_barriers[i].is_available = true
			else
				-- objekt posunieme 
				all_barriers[i]:translate( 0, (speed / 2) * delta_time )
			end
		end
	end
end

function increase_speed()
	if speed >= max_speed then return end

	if speed > 10 then
		speed = speed + 0.001
	elseif speed > 8 then
		speed = speed + 0.003
	else
		speed = speed + 0.008
	end
end

function increase_score()
	score = score + speed / 100

	local rounded_score = math.round(score)
	if rounded_score ~= last_score then
		-- update score textu iba ak je potrebne
		score_text.text = math.round(score) .. 'm'
	end
end

function start_barrier_position()
	-- kolko fragmentov sa bude cakat
	number_of_fragments = math.random(3)
	
	wall_position = math.random(2)
	if wall_position < 1 then
		wall_position = 'left'
	else
		wall_position = 'right'
	end

	barrier_space = 60
	current_barrier_space = start_barrier_space
end

function next_barrier_position()
	-- kolko fragmentov sa bude cakat
	number_of_fragments = math.random(3)
	
	if wall_position == 'left' then
		wall_position = 'right'
	else
		wall_position = 'left'
	end

	if current_barrier_space > min_barrier_space then
		current_barrier_space = current_barrier_space - 1
	end
end

function get_free_barrier()
	local object

	while true do
		-- takuto barieru chceme emitovat
		local barrier_type = barrier_definition[math.random(#barrier_definition)].type

		-- zistime, ci mame volny objekt
		for i = 1, 3 do
			local _object = barrier_image[wall_position][barrier_type][i]
			if _object.is_available then
				object = _object
				break
			end
		end

		if object ~= nil then
			-- mame objekt
			break
		end
	end

	-- mame novy objekt
	return object
end

function create_barrier()
	-- robime medzeru
	if last_barrier ~= nil and last_barrier.y < barrier_space then
		-- nerobime nic, aby neboli moc na husto
		return
	end

	if number_of_fragments < 1 then
		-- vygenerujeme nove pozicie
		next_barrier_position()
	end

	-- aku barieru budeme davat?
	last_barrier = get_free_barrier()
	last_barrier.is_available = false

	-- nastavime novu start poziciu
	last_barrier.y = -last_barrier.height

	number_of_fragments = number_of_fragments - 1

	if number_of_fragments < 1 then
		-- zvacsime medzeru, aby sa dalo preskocit
		barrier_space = current_barrier_space
	else
		barrier_space = math.random(15, 25)
	end
end

function get_aqua( source )
	local selected = nil

	for i = 1, aqua_count do
		if source[i].isBodyActive ~= true then
			selected = source[i]
			break
		end
	end

	return selected
end

function create_aqua()
	local x = math.random( screenW - 50 ) + 25
	local s = math.random( 10 )

	local _aqua 
	local _d

	if s > 5 then
		_aqua = get_aqua( small_aqua )
		_d = s * 2
	else
		_aqua = get_aqua( big_aqua )
		_d = s / 2
	end

	if _aqua ~= nil then
		_aqua.x = x

		physics.addBody( _aqua, 'dynamic', {density = _d, friction = 0, bounce = 0} )
	end

	create_aqua_timer = timer.performWithDelay( 700, create_aqua )
end

function background_touch_handler( self, event )
	if event.phase ~= 'began' then return end

	sounds.play(sounds.sound.jump, sounds.channel.PLAYER_CHANNEL)

	if climber.on_wall == 'right' then
		climber.on_wall = 'left'
		climber.x = 98
		climber:setSequence('left')
		climber:play()
	else
		climber.on_wall = 'right'
		climber.x = screenW - 98
		climber:setSequence('right')
		climber:play()
	end
end

function create_background()
	-- najprv odstranim stary background
	if mountain_background ~= nil then
		mountain_background:removeSelf()
		mountain_background = nil
	end

	-- background je hora, ktora sa vzdy nahodne bude menit
	mountain_background = common.background()
	background_group:insert( mountain_background )

	mountain_background_height = mountain_background.height
end

function create_wall(group)
	-- prvy fragment je umiestneny presne na obrazovku
	wall_left_1 = display.newImageRect( 'images/wall-left.png', 75, screenH )
	wall_left_1.anchorX = 0
	wall_left_1.anchorY = 0
	wall_left_1.x = -15
	wall_left_1.y = 0
	group:insert( wall_left_1 )

	-- druhy fragment je umiestneny pod nim
	wall_left_2 = display.newImageRect( 'images/wall-left.png', 75, screenH )
	wall_left_2.anchorX = 0
	wall_left_2.anchorY = 0
	wall_left_2.x = -15
	wall_left_2.y = -screenH
	group:insert( wall_left_2 )

	-- prvy fragment je umiestneny presne na obrazovku
	wall_right_1 = display.newImageRect( 'images/wall-right.png', 75, screenH )
	wall_right_1.anchorX = 1
	wall_right_1.anchorY = 0
	wall_right_1.x = screenW + 15
	wall_right_1.y = 0
	group:insert( wall_right_1 )

	-- prvy fragment je umiestneny presne na obrazovku
	wall_right_2 = display.newImageRect( 'images/wall-right.png', 75, screenH )
	wall_right_2.anchorX = 1
	wall_right_2.anchorY = 0
	wall_right_2.x = screenW + 15
	wall_right_2.y = -screenH
	group:insert( wall_right_2 )
end

function climber_collision_handler( self, event )
	if event.phase ~= 'began' then return end

	-- hra uz neprebieha
	if game_running == false then return end

	if event.other.object_name == 'barrier' then
		collision_with_barrier( event.other )
	elseif event.other.object_name == 'helmet' then
		collision_with_helmet( event.other )
	end
end

function collision_with_barrier( object )
	if have_helmet then
		-- ak ma helmu, tak sa nic nedeje
		sounds.play(sounds.sound.hit, sounds.channel.SFX_CHANNEL)

		have_helmet = false
		update_helmet_indicator()

		utils.nextFrame(function ()
			object.y = -100
			object.is_available = true
		end)
	else
		-- pustime zvuk
		sounds.play(sounds.sound.haaa, sounds.channel.SFX_CHANNEL)

		-- prestanem animovat lezca
		climber:pause( )

		-- prestanem emitovat bariery
		game_running = false

		-- prestanem emitovat kvapky
		timer.cancel( create_aqua_timer )

		-- lezec sa odpingne od steny
		climber.gravityScale = 1

		if climber.on_wall == 'left' then
			climber:setLinearVelocity( 100, -160 )
		else
			climber:setLinearVelocity( -100, -160 )
		end

		-- dokoncim ukoncenie hry
		stop_game()
	end

	-- zatrasieme screen
	shake.start()
	timer.performWithDelay( 400, function ( ... )
		shake.stop()
	end)
end

function collision_with_helmet( object )
	have_helmet = true
	update_helmet_indicator()

	sounds.play(sounds.sound.collect, sounds.channel.SFX_CHANNEL)

	utils.nextFrame(function ()
		physics.removeBody( object )
		object.y = -100
	end)
end

function create_climber( group )
	-- climber
	local sheetWidth = 220
	local sheetHeight = 350
	local imageSheetOptions = {
		width = sheetWidth / 2,
		height = sheetHeight / 2,
		numFrames = 4,
		sheetContentWidth = sheetWidth,
		sheetContentHeight = sheetHeight
	}

	local imageSheet = graphics.newImageSheet( 'images/climber.png', imageSheetOptions )

	climber = display.newSprite( imageSheet, {
		{name = 'left', start = 1, count = 2, time = 400},
		{name = 'right', start = 3, count = 2, time = 400},
	} )

	climber.on_wall = 'left'
	climber.x = 98
	climber.y = screenH - 140

	group:insert(climber)

	climber:setSequence('left')
	climber:play()

	physics.addBody( climber, 'dynamic', { bounce = 0, isSensor = true, filter = { categoryBits = 1, maskBits = 6 }})
	climber.isSleepingAllowed = false
	climber.gravityScale = 0

	utils.handlerAdd( climber, 'collision', climber_collision_handler )
end

function start_game()
	score = 0  -- vynulujeme score
	have_helmet = false  -- pri starte nema helmu

	physics.start( )
	physics.setGravity( 0, 10 )
	--physics.setDrawMode( 'hybrid' )

	update_helmet_indicator()

	cloud_position = start_cloud_position  -- oblaky sa zacnu generovat hore

	-- updateneme pozadie()
	create_background()

	create_climber(climber_group)

	-- vsetky bariery pridame do fyziky
	for i = 1, all_barriers_count do
		physics.addBody( all_barriers[i], 'static', { bounce = 0, isSensor = true, filter = { categoryBits = 2, maskBits = 1 }})
	end
		
	-- if settings.music then
	-- 	audio.play( sounds.music_sound, {channel = MUSIC_CHANNEL, loops = -1} )
	-- 	audio.setVolume( 0.75, {channel = MUSIC_CHANNEL} )
	-- end

	create_aqua()

	-- postupne zvysujeme rychlost
	speed = start_speed
	increase_speed()

	-- posuvame stenu
	get_delta_time()  -- aby sme spravne pocitali casovy posun
	Runtime:addEventListener( "enterFrame", enter_frame_handler )

	-- spustime ovladanie
	utils.handlerAdd(mountain_background, 'touch', background_touch_handler)

	-- povolime vytvaranie barier
	start_barrier_position()
	game_running = true

	emit_falling_objet_timer = timer.performWithDelay(10000 + math.random(2000), falling_object)  -- padajuce objekty
	emit_helicopter_timer = timer.performWithDelay(10000 + math.random(2000), emit_helicopter)  -- helikoptera
	emit_helmet_timer = timer.performWithDelay(25000 + math.random(5000), emit_helmet)  -- helma
	
	emit_cloud_timer = emit_cloud()  -- emitujeme oblaky

	-- reset pozicie jelena
	jelen_image.x = -100
	jelen_image.y = -100

	jelen_out = false
	emit_jelen_timer = timer.performWithDelay(25000 + math.random(5000), emit_jelen)  -- jelen
end

function stop_emit()
	if emit_falling_objet_timer ~= nil then timer.cancel(emit_falling_objet_timer) end
	if emit_helicopter_timer ~= nil then timer.cancel(emit_helicopter_timer) end
	if emit_helmet_timer ~= nil then timer.cancel(emit_helmet_timer) end
	if emit_cloud_timer ~= nil then timer.cancel(emit_cloud_timer) end
	if emit_jelen_timer ~= nil then timer.cancel(emit_jelen_timer) end
end

function stop_game()
	-- zastavime posuvanie steny
	Runtime:removeEventListener( "enterFrame", enter_frame_handler )

	-- zastavime ovladanie
	utils.handlerRemove(mountain_background, 'touch')

	-- prestaneme emitovat
	stop_emit()
	
	timer.performWithDelay( 2500, function ( ... )
		-- odstranim vsetky kvapky
		for i = 1, #aqua do
			display.remove(aqua[i])
			aqua[i] = nil
		end

		aqua = {}

		-- odstranim lezca
		physics.removeBody( climber )
		climber:removeSelf( )
		climber = nil

		-- a koniec
		physics.stop( )

		-- game over screen
		composer.gotoScene('game_over', {
			params = {
				score = math.round(score)
			}
		})
	end )
end

function enter_frame_handler( event )
	if not game_running then return end

	delta_time = get_delta_time()

	move_background()
	move_wall()
	move_barrier()
	create_barrier()
	
	increase_speed()
	increase_score()
end

function scene:create( event )
	local sceneGroup = self.view

	background_group = display.newGroup()
	sceneGroup:insert( background_group )
	create_background()

	-- kvapy idu pred stenou
	back_group = display.newGroup( )
	sceneGroup:insert( back_group )

	create_wall(sceneGroup)

	barrier_group = display.newGroup( )
	sceneGroup:insert( barrier_group )

	climber_group = display.newGroup( )
	sceneGroup:insert( climber_group )

	-- dalsie kvapky idu pred lezcom
	front_group = display.newGroup( )
	sceneGroup:insert( front_group )

	-- aqua bude obsahovat vsetky kvapky aby sa dali odstranit
	aqua = {}

	-- score background
	local bg_score = display.newImageRect( back_group, 'images/background-score.png', 77, 48 )
	bg_score.anchorX = 0
	bg_score.anchorY = 0
	bg_score.x = 0
	bg_score.y = 6
	sceneGroup:insert( bg_score )

	-- score
	score_text = display.newEmbossedText( {
		text = '',
		font = common.DEFAULT_FONT,
		fontSize = 24,
		x = 0,
		y = 15,
		align = 'right',
		width = 66,
	} )
	score_text:setEmbossColor( common.EMBOSS_COLOR )
	score_text.anchorX = 0
	score_text.anchorY = 0
	sceneGroup:insert( score_text )

	-- helmet indicator
	local bg_helmet = display.newImageRect( back_group, 'images/background-helmet.png', 832 / 16, 761 / 16 )
	bg_helmet.anchorX = 1
	bg_helmet.anchorY = 0
	bg_helmet.x = screenW
	bg_helmet.y = 6
	sceneGroup:insert( bg_helmet )

	helmet_status_off = display.newImageRect( back_group, 'images/helmet-bw.png', 50 / 2, 52 / 2 )
	helmet_status_off.anchorX = 1
	helmet_status_off.anchorY = 0
	helmet_status_off.x = screenW - 8
	helmet_status_off.y = 13
	helmet_status_off.alpha = 0.8
	sceneGroup:insert( helmet_status_off )

	helmet_status_on = display.newImageRect( back_group, 'images/helmet.png', 50 / 2, 52 / 2 )
	helmet_status_on.anchorX = 1
	helmet_status_on.anchorY = 0
	helmet_status_on.x = screenW - 8
	helmet_status_on.y = 13
	helmet_status_on.isVisible = false
	sceneGroup:insert( helmet_status_on )

	preload_images()
	init_physics_body()
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		ga:view('game')

		-- reset barriers postition§
		for i = 1, all_barriers_count do
			all_barriers[i].y = -100
			all_barriers[i].is_available = true
		end

		last_barrier = nil
	elseif phase == "did" then
		start_game()

		-- reset physiscs bodies for preloaded images
		reset_physics_body()
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		
	elseif phase == "did" then
		-- Called when the scene is now off screen
		-- if settings.music then
		-- 	audio.fadeOut( MUSIC_CHANNEL, 1500 )
		-- end

		-- ak nahodou este prebieha trasenie
		shake.stop()

		-- zastavime emitovanie
		stop_emit()
	end	
end

function scene:destroy( event )
	local sceneGroup = self.view

end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene