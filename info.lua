local composer = require( "composer" )
local scene = composer.newScene()

local physics = require( "physics" )
local widget = require( "widget" )

local common = require( "common" )

local bg
local audioSwitch

function scene:create( event )
	utils.debug('info:scene:create')

	-- initialize the scene
	local sceneGroup = self.view

	-- background
	mountain_background = common.background()
	sceneGroup:insert( mountain_background )

	local background = display.newRect(halfW, halfH, screenW, screenH)
	background:setFillColor(utils.color(0, 0, 0))
	background.alpha = 0.65
	sceneGroup:insert( background )

	-- game name
	local game_name = display.newEmbossedText( {
		text = R.strings('jarda_in_tatras'),
		font = common.DEFAULT_FONT,
		fontSize = 44,
		x = halfW,
		y = 28
	} )
	game_name:setEmbossColor( common.EMBOSS_COLOR )
	sceneGroup:insert( game_name )

	-- how to survive
	local how_to_survive = display.newEmbossedText( {
		text = R.strings('how_to_survive'),
		font = common.DEFAULT_FONT,
		fontSize = 30,
		x = halfW,
		y = 66
	} )
	how_to_survive:setEmbossColor( common.EMBOSS_COLOR )
	sceneGroup:insert( how_to_survive )

	-- table with hints
	local info = R.arrays('info')
	local info_text_objects = {}

	local function get_table_row( row_index )
		local info_text = display.newEmbossedText( {
			text = info[row_index],
			font = common.DEFAULT_FONT,
			fontSize = 25,
			x = 5,
			y = 7,
			width = screenW - 30,
			align = 'center'
		} )
		info_text:setEmbossColor( common.EMBOSS_COLOR )
		info_text.anchorX = 0
		info_text.anchorY = 0

		return info_text
	end

	local table = widget.newTableView({
		left = 10,
		top = 90,
		width = screenW - 20,
		height = screenH - 90 - 74,
		hideBackground = true,
		onRowRender = function ( event )
			row = event.row
			row:insert(get_table_row( row.index ))
		end
	})
	sceneGroup:insert( table )

	for i = 1, #info do
		local info_text = get_table_row( i )

		local rowColor = { default={ 1, 0.5, 0, 0 }, over={ 1, 1, 1, 0.1 } }
		local lineColor = { 233 / 255, 223 / 255, 184 / 255, 0 }

	    table:insertRow({
	    	rowHeight = info_text.height + 14,
			rowColor = rowColor,
			lineColor = lineColor,
            data = {}
	    })

	    info_text:removeSelf()
	    info_text = nil
	end

	-- main menu
	local menu_group = display.newGroup( )
	sceneGroup:insert( menu_group )

	local y = screenH - 35
	local menu_button = display.newImageRect( menu_group, 'images/menu.png', 54, 54 )
	menu_button.x = halfW - 40
	menu_button.y = y

	local menu_label = display.newEmbossedText( {
		text = R.strings('main_menu'),
		font = common.DEFAULT_FONT,
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	menu_label:setEmbossColor( common.EMBOSS_COLOR )
	menu_label.anchorX = 0
	menu_group:insert( menu_label )

	utils.handlerAdd(menu_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( menu_group )
		elseif event.phase == 'ended' and common.button_up( menu_group ) then
			composer.gotoScene( 'menu' )
		end

		return true
	end )
end

function scene:show( event )
	if event.phase == 'will' then
		ga:view('info')
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)

	elseif phase == "did" then
		
	end	
	
end

function scene:destroy( event )
	local sceneGroup = self.view

end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene