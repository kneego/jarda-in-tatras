local composer = require( "composer" )
local scene = composer.newScene()

local physics = require( "physics" )
local widget = require( "widget" )

local common = require( "common" )

local bg
local audioSwitch

local score_text
local best_score_text
local new_best_score_label

function scene:create( event )
	utils.debug('menu:scene:create')

	-- initialize the scene
	local sceneGroup = self.view

	-- background
	mountain_background = common.background()
	sceneGroup:insert( mountain_background )

	local background = display.newRect(halfW, halfH, screenW, screenH)
	background:setFillColor(utils.color(0, 0, 0))
	background.alpha = 0.65
	sceneGroup:insert( background )

	-- replay
	local replay_group = display.newGroup( )
	sceneGroup:insert( replay_group )

	local y = screenH - 170
	local replay_button = display.newImageRect( replay_group, 'images/replay.png', 54, 54 )
	replay_button.x = halfW - 40
	replay_button.y = y

	local replay_label = display.newEmbossedText( {
		text = R.strings('replay'),
		font = common.DEFAULT_FONT,
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	replay_label:setEmbossColor( common.EMBOSS_COLOR )
	replay_label.anchorX = 0
	replay_group:insert( replay_label )

	utils.handlerAdd(replay_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( replay_group )
		elseif event.phase == 'ended' and common.button_up( replay_group ) then
			composer.gotoScene( 'game', {
				params = {
					level = level_number
				}
			} )
		end

		return true
	end )

	-- leadeboard
	local leaderboard_group = display.newGroup( )
	sceneGroup:insert( leaderboard_group )

	local y = screenH - 110
	local leaderboard_button = display.newImageRect( leaderboard_group, 'images/leaderboard.png', 54, 54 )
	leaderboard_button.x = halfW - 40
	leaderboard_button.y = y

	local leaderboard_label = display.newEmbossedText( {
		text = R.strings('leaderboard'),
		font = common.DEFAULT_FONT,
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	leaderboard_label:setEmbossColor( common.EMBOSS_COLOR )
	leaderboard_label.anchorX = 0
	leaderboard_group:insert( leaderboard_label )

	utils.handlerAdd( leaderboard_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( leaderboard_group )
		elseif event.phase == 'ended' and common.button_up( leaderboard_group ) then
			game_network.show_leaderboards()
		end

		return true
	end )

	-- main menu
	local menu_group = display.newGroup( )
	sceneGroup:insert( menu_group )

	local y = screenH - 50
	local menu_button = display.newImageRect( menu_group, 'images/menu.png', 54, 54 )
	menu_button.x = halfW - 40
	menu_button.y = y

	local menu_label = display.newEmbossedText( {
		text = R.strings('main_menu'),
		font = common.DEFAULT_FONT,
		fontSize = 30,
		x = halfW - 5,
		y = y
	} )
	menu_label:setEmbossColor( common.EMBOSS_COLOR )
	menu_label.anchorX = 0
	menu_group:insert( menu_label )

	utils.handlerAdd(menu_group, 'touch', function ( self, event )
		if event.phase == 'began' then
			common.button_down( menu_group )
		elseif event.phase == 'ended' and common.button_up( menu_group ) then
			composer.gotoScene( 'menu' )
		end

		return true
	end )

	-- score
	local score_label = display.newEmbossedText( {
		text = R.strings('score'),
		font = common.DEFAULT_FONT,
		fontSize = 30,
		x = halfW - 5,
		y = halfH - 50
	} )
	score_label:setEmbossColor( common.EMBOSS_COLOR )
	score_label.anchorX = 1
	sceneGroup:insert( score_label )

	score_text = display.newEmbossedText( {
		text = '',
		font = common.DEFAULT_FONT,
		fontSize = 40,
		x = halfW + 5,
		y = halfH - 52
	} )
	score_text:setEmbossColor( common.EMBOSS_COLOR )
	score_text.anchorX = 0
	sceneGroup:insert( score_text )

	-- best score
	local best_score_label = display.newEmbossedText( {
		text = R.strings('your_best'),
		font = common.DEFAULT_FONT,
		fontSize = 30,
		x = halfW - 5,
		y = halfH
	} )
	best_score_label:setEmbossColor( common.EMBOSS_COLOR )
	best_score_label.anchorX = 1
	sceneGroup:insert( best_score_label )

	best_score_text = display.newEmbossedText( {
		text = '',
		font = common.DEFAULT_FONT,
		fontSize = 40,
		x = halfW + 5,
		y = halfH - 2
	} )
	best_score_text:setEmbossColor( common.EMBOSS_COLOR )
	best_score_text.anchorX = 0
	sceneGroup:insert( best_score_text )

	-- new best score
	new_best_score_label = display.newEmbossedText( {
		text = R.strings('new_best_score'),
		font = common.DEFAULT_FONT,
		fontSize = 40,
		x = halfW,
		y = halfH - 140
	} )
	new_best_score_label:setEmbossColor( common.EMBOSS_COLOR )
	new_best_score_label.isVisible = false
	sceneGroup:insert( new_best_score_label )
end

function scene:show( event )
	if event.phase == 'will' then
		score_text.text = event.params.score

		-- este achievements odomknem ak je to potrebne
		local _achievement = R.arrays('achievements')['Android']
		for k,v in pairs(_achievement) do
			if tonumber(event.params.score) > tonumber(k) then
				game_network.unlock_achievement(v)
			end
		end

		if event.params.score > settings.best_score then
			sounds.play(sounds.sound.win, sounds.channel.SFX_CHANNEL)

			new_best_score_label.isVisible = true

			settings.best_score = event.params.score
			utils.saveSettings(settings)
		else
			new_best_score_label.isVisible = false
		end

		best_score_text.text = settings.best_score

		ga:view('game_over')
	elseif event.phase == 'did' then
		if ads.isLoaded then
			-- zobrazime reklamu
			ads.show( 'interstitial', { x = 0, y = 0, testMode = false } )

			-- a naloadujeme dalsiu
			ads.load( "interstitial", { testMode = false } )
		end

		if is_emulator then
			utils.debug('game_over:send score', event.params.score)
		else
			game_network.send_score(R.arrays('board_id')[platform_name], event.params.score)
		end
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	
	local phase = event.phase
	
	if event.phase == "will" then
		-- pause the scene (stop timers, stop animation, unload sounds, etc.)

	elseif phase == "did" then

	end	
	
end

function scene:destroy( event )
	local sceneGroup = self.view

end

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene