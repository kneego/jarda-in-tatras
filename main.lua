require( 'knee.globals' )
require( 'common' )

-- data = require( "data" )
ads = require( 'ads' )
utils = require( 'knee.utils' )
preload = require( 'knee.preload' )
back_button = require( 'knee.back_button' )

display.setStatusBar( display.HiddenStatusBar )
display.setDefault( 'background', 0, 0, 0 )
display.setDefault( 'fillColor', utils.color(255, 255, 255) )

if audio.supportsSessionProperty == true then
	audio.setSessionProperty(audio.MixMode, audio.AmbientMixMode)
end

debugging = false

if debugging then
	require( 'knee.screen_capture' )  -- aby sme vedeli robit screenshoty (klaves S)
	utils.get_fonts_names()  -- aby sme vedeli nazvy fontov

	-- language = 'sk'  -- testujeme rozne jazyky
end

game_network = require( 'knee.game_network' )
game_network.debugging = false

game_network.load_local_player_callback = function (event)
	if event ~= nil and event.data ~= nil then
	end
end

game_network.init()

-- rate dialog
rate = require( 'knee.rate' )
rate.init({
	android_rate = 'market://details?id=org.keego.jardaintatras',
	ios_rate = 'itms://itunes.apple.com/us/app/jarda-in-tatras/id1068434722?ls=1&mt=8',
	times_used = 5,
	days_used = 10,
	version = app_version,
	remind_times = 5,
	remind_days = 10,
	rate_title = 'Rate Jarda',
	rate_text = 'If you enjoy this game, please take a moment to rate it. Thank you for your support!',
	rate_button = 'Rate now',
	remind_button = 'Remind later',
	cancel_button = 'No, thanks',
})

-- resources (strings, arrays)
R = require( 'resources' )
sounds = require('sounds')

ga = require( 'knee.ga' )
ga:init( 'UA-39791763-25' )

-- reklamu zobraujeme inu pre iOS a inu pre Android
ads.init( 'admob', R.arrays('admob_id')[platform_name], function( event )
	utils.debug('phase:', event.phase, ', type:', event.type, ', name:', event.name, ', error:', event.isError, ', response:', event.response)
end )

back_button.init()

-- load settings
settings = utils.loadSettings({
	-- default empty data
	emptyData = {
		unlockedTo = 1,
		sounds = true,
		music = true,
		show_help = true,
		game_play_counter = 0,
		levels = {},
		best_score = 0,
	}
})

-- preloading
preload.start()

-- load menu screen
local composer = require( 'composer' )

composer.gotoScene( 'menu' )
