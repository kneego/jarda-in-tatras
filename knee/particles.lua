local json = require('json')

local _M = {
	_particle_data = {}
}

function _M.get_emiter( file_name )
	if _M._particle_data[file_name] == nil then
		-- load data from file
		local path = system.pathForFile( file_name )
		local f = io.open( path, "r" )
	   	local file_data = f:read( "*a" )
	   	f:close()

	   	_M._particle_data[file_name] = json.decode( file_data )
	end

   	local emitter = display.newEmitter( _M._particle_data[file_name] )

   	return emitter
end

return _M