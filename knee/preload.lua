local _M = {}

function _M.start()
	local preload = {
        'aqua-big.png',
        'aqua-small.png',
        'audio.png',
        'background-helmet.png',
        'background-score.png',
        'barrier-stone-1-left.png',
        'barrier-stone-3-left.png',
        'barrier-stone-4-left.png',
        'barrier-stone-1-right.png',
        'barrier-stone-3-right.png',
        'barrier-stone-4-right.png',
        'barrier-wood-2-left.png',
        'barrier-wood-5-left.png',
        'barrier-wood-2-right.png',
        'barrier-wood-5-right.png',
        'climber.png',
        'cloud-1.png',
        'cloud-2.png',
        'cloud-3.png',
        'facebook.png',
        'falling-helicopter.png',
        'falling-stone-1.png',
        'falling-stone-2.png',
        'falling-tourist.png',
        'helicopter.png',
        'helmet.png',
        'helmet-bw.png',
        'info.png',
        'leaderboard.png',
        'menu.png',
        'mountains.png',
        'play.png',
        'replay.png',
        'twitter.png',
        'ufo.png',
        'wall-left.png',
        'wall-right.png',
    }

	-- start loading
	local background = display.newRect(halfW, halfH, screenW, screenH)
	background:setFillColor(utils.color(35, 25, 45))

	for i = 1, #preload do
		local object = display.newImageRect( 'images/' .. preload[i], 64, 64 )
		object:removeSelf()
		object = nil
	end

	background:removeSelf()
	background = nil
end

return _M