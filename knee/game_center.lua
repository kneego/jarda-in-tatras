function setHighScoreFinished( event )
	hideSpinner()

	if event.errorCode then
		-- something went wrong	
		native.showAlert( R.strings('fail'), R.strings('errorSendingScore'), {  } )
	end

	storyboard.gotoScene( "score", "fade", 400 )
end

function setHighScore()
	gameNetwork.request( "setHighScore",
	{
	    localPlayerScore = { category=R.strings('boardId'), value=data['score'] },
	    listener = setHighScoreFinished
	})
end

function gcInitCallback( event )
	if gameActivity then
		if event.errorCode then
			-- something went wrong
			hideSpinner()
			native.showAlert( R.strings('fail'), R.strings('errorLoggingIn'), { R.strings('ok') } )
			return false
		end

	    if event.data then
	        setHighScore()
	    else
	        showSpinner()
	    end
	end
end

function sendScoreToServer()
	showSpinner()

	if not gameNetwork.init( "gamecenter", gcInitCallback ) then
		-- user is logged in to game center
		setHighScore()
	end
end